public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("trần văn ninh");
        // khởi tạo đối tượng hình tròn không tham số
        Circle circle = new Circle();
        // khỏi tạo đối tượng hình tròn có tham số
        Circle circle2 = new Circle(5);
        System.out.println("diện tích hình tròn là " + circle.getArea());
        System.out.println("chu vi hình tròn là " + circle.getPerimeter());
        System.out.println("diện tích hình tròn là " + circle2.getArea());
        System.out.println("chu vi hình tròn là " + circle2.getPerimeter());
        System.out.println(circle.toString());
        System.out.println(circle2.toString());
    }
}
