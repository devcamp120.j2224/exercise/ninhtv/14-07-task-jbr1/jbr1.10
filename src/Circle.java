public class Circle {
    double radius = 1.0;
    // phương thức khởi tạo không tham số
    public Circle() {
        //super();
    }
    // phương thức khởi tạo có tham số
    public Circle(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    //phương thức tính diện tích hình tròn
    public double getArea(){
        return Math.PI * (radius * radius);
    }
    //phương thức tính chu vi hình tròn
    public double getPerimeter(){
        return Math.PI * (2 * radius);
    }
    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }
    
}
